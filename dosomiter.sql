-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 18, 2017 at 12:57 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dosomiter`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_access_log`
--

CREATE TABLE `tbl_access_log` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `ip_address` text COLLATE utf8_unicode_ci NOT NULL,
  `device` text COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8_unicode_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_access_log`
--

INSERT INTO `tbl_access_log` (`ID`, `user_id`, `ip_address`, `device`, `user_agent`, `date`) VALUES
(1, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-05 17:43:59'),
(2, 1, '::1', 'Desktop/Laptop', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36', '2017-03-21 17:01:21'),
(3, 1, '127.0.0.1', 'Desktop/Laptop', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.1 Safari/603.1.30', '2017-04-17 22:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_acquisition_types`
--

CREATE TABLE `tbl_acquisition_types` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_acquisition_types`
--

INSERT INTO `tbl_acquisition_types` (`ID`, `name`, `add_date`) VALUES
(1, 'Test Name', '2017-03-05 18:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_centres`
--

CREATE TABLE `tbl_centres` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_centres`
--

INSERT INTO `tbl_centres` (`ID`, `name`, `add_date`) VALUES
(1, 'Test Centre', '2017-03-05 18:07:46'),
(2, 'Test Centre', '2017-03-05 18:08:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dose_alerts`
--

CREATE TABLE `tbl_dose_alerts` (
  `ID` bigint(20) NOT NULL,
  `centre_id` bigint(20) NOT NULL,
  `protocol_spec_id` bigint(20) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `value` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_dose_alerts`
--

INSERT INTO `tbl_dose_alerts` (`ID`, `centre_id`, `protocol_spec_id`, `name`, `value`, `description`, `add_date`) VALUES
(1, 12345, 12345, 'Test', 'Test', 'Test', '2017-03-21 17:08:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_equipments`
--

CREATE TABLE `tbl_equipments` (
  `ID` bigint(20) NOT NULL,
  `modality_id` bigint(20) NOT NULL,
  `manufacturer` varchar(1024) NOT NULL,
  `model` varchar(1024) NOT NULL,
  `serial_no` varchar(1024) DEFAULT NULL,
  `software_version` varchar(1024) DEFAULT NULL,
  `station_name` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_equipments`
--

INSERT INTO `tbl_equipments` (`ID`, `modality_id`, `manufacturer`, `model`, `serial_no`, `software_version`, `station_name`, `add_date`) VALUES
(1, 12345, 'Test', 'Test', 'Test', 'Test', 'Test', '2017-03-21 17:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

CREATE TABLE `tbl_images` (
  `ID` bigint(20) NOT NULL,
  `series_id` bigint(20) NOT NULL,
  `image_uid` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_images`
--

INSERT INTO `tbl_images` (`ID`, `series_id`, `image_uid`, `add_date`) VALUES
(1, 12345, 'Test', '2017-03-21 17:05:13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image_dose_attributes`
--

CREATE TABLE `tbl_image_dose_attributes` (
  `ID` bigint(20) NOT NULL,
  `image_id` bigint(20) NOT NULL,
  `att_type` bigint(20) NOT NULL,
  `value` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_image_dose_attributes`
--

INSERT INTO `tbl_image_dose_attributes` (`ID`, `image_id`, `att_type`, `value`, `add_date`) VALUES
(1, 12345, 12345, 'Test', '2017-03-21 17:05:26');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_image_dose_attribute_descriptions`
--

CREATE TABLE `tbl_image_dose_attribute_descriptions` (
  `ID` bigint(20) NOT NULL,
  `description` text NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_image_dose_attribute_descriptions`
--

INSERT INTO `tbl_image_dose_attribute_descriptions` (`ID`, `description`, `add_date`) VALUES
(1, 'This is test', '2017-03-05 18:26:08');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modalities`
--

CREATE TABLE `tbl_modalities` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(15) NOT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_modalities`
--

INSERT INTO `tbl_modalities` (`ID`, `name`, `add_date`) VALUES
(1, 'Test', '2017-03-05 18:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notifications`
--

CREATE TABLE `tbl_notifications` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `title` text NOT NULL,
  `notification` text NOT NULL,
  `read` int(1) NOT NULL DEFAULT '0',
  `hide` int(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notifications`
--

INSERT INTO `tbl_notifications` (`ID`, `user_id`, `title`, `notification`, `read`, `hide`, `date`) VALUES
(178, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 0, 0, '2017-03-05 17:44:56'),
(179, 1, 'Users Capabilities updated', 'You have successfully updated users capabilities.', 0, 0, '2017-03-05 17:45:03');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_options`
--

CREATE TABLE `tbl_options` (
  `ID` bigint(20) NOT NULL,
  `option_name` text NOT NULL,
  `option_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_options`
--

INSERT INTO `tbl_options` (`ID`, `option_name`, `option_value`) VALUES
(13, 'users_capabilities', 's:1631:"a:3:{s:5:"admin";a:18:{s:9:"view_user";i:0;s:8:"add_user";i:0;s:9:"edit_user";i:0;s:22:"view_acquisition_types";i:0;s:12:"view_centres";i:0;s:24:"view_ct_study_attributes";i:0;s:23:"view_ct_study_exposures";i:0;s:15:"view_equipments";i:0;s:11:"view_images";i:0;s:26:"view_image_dose_attributes";i:0;s:38:"view_image_dose_attribute_descriptions";i:0;s:25:"view_mg_series_attributes";i:0;s:15:"view_modalities";i:0;s:13:"view_patients";i:0;s:13:"view_phantoms";i:0;s:11:"view_series";i:0;s:12:"view_studies";i:0;s:17:"view_xray_sources";i:0;}s:16:"hospital_manager";a:18:{s:9:"view_user";i:1;s:8:"add_user";i:1;s:9:"edit_user";i:0;s:22:"view_acquisition_types";i:0;s:12:"view_centres";i:0;s:24:"view_ct_study_attributes";i:0;s:23:"view_ct_study_exposures";i:0;s:15:"view_equipments";i:0;s:11:"view_images";i:0;s:26:"view_image_dose_attributes";i:0;s:38:"view_image_dose_attribute_descriptions";i:0;s:25:"view_mg_series_attributes";i:0;s:15:"view_modalities";i:0;s:13:"view_patients";i:0;s:13:"view_phantoms";i:0;s:11:"view_series";i:0;s:12:"view_studies";i:0;s:17:"view_xray_sources";i:0;}s:12:"general_user";a:18:{s:9:"view_user";i:1;s:8:"add_user";i:1;s:9:"edit_user";i:0;s:22:"view_acquisition_types";i:0;s:12:"view_centres";i:0;s:24:"view_ct_study_attributes";i:0;s:23:"view_ct_study_exposures";i:0;s:15:"view_equipments";i:0;s:11:"view_images";i:0;s:26:"view_image_dose_attributes";i:0;s:38:"view_image_dose_attribute_descriptions";i:0;s:25:"view_mg_series_attributes";i:0;s:15:"view_modalities";i:0;s:13:"view_patients";i:0;s:13:"view_phantoms";i:0;s:11:"view_series";i:0;s:12:"view_studies";i:0;s:17:"view_xray_sources";i:0;}}";');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_patients`
--

CREATE TABLE `tbl_patients` (
  `ID` bigint(20) NOT NULL,
  `hospital_id` bigint(20) DEFAULT NULL,
  `gender` varchar(1024) DEFAULT NULL,
  `yob` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_patients`
--

INSERT INTO `tbl_patients` (`ID`, `hospital_id`, `gender`, `yob`, `add_date`) VALUES
(1, 123, 'Male', '1992', '2017-03-05 18:10:58');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_phantoms`
--

CREATE TABLE `tbl_phantoms` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `size` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_phantoms`
--

INSERT INTO `tbl_phantoms` (`ID`, `name`, `size`, `add_date`) VALUES
(1, 'Test', '20', '2017-03-05 18:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_protocol`
--

CREATE TABLE `tbl_protocol` (
  `ID` bigint(20) NOT NULL,
  `protocol_spec_id` varchar(1024) DEFAULT NULL,
  `name` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_protocol_specs`
--

CREATE TABLE `tbl_protocol_specs` (
  `ID` bigint(20) NOT NULL,
  `name` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_protocol_specs`
--

INSERT INTO `tbl_protocol_specs` (`ID`, `name`, `add_date`) VALUES
(1, 'Test', '2017-03-21 17:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_series`
--

CREATE TABLE `tbl_series` (
  `ID` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `equipment_id` bigint(20) NOT NULL,
  `series_uid` varchar(64) DEFAULT NULL,
  `series_date` varchar(1024) DEFAULT NULL,
  `description` text,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_series`
--

INSERT INTO `tbl_series` (`ID`, `study_id`, `equipment_id`, `series_uid`, `series_date`, `description`, `add_date`) VALUES
(1, 123456, 123456, 'Test', '2016-10-12', 'Test', '2017-03-21 17:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_studies`
--

CREATE TABLE `tbl_studies` (
  `ID` bigint(20) NOT NULL,
  `patient_id` bigint(20) NOT NULL,
  `centre_id` bigint(20) NOT NULL,
  `study_uid` varchar(64) DEFAULT NULL,
  `study_date` varchar(1024) DEFAULT NULL,
  `study_description` text,
  `from_rdsr` varchar(1024) DEFAULT NULL,
  `patient_size` varchar(1024) DEFAULT NULL,
  `patient_weight` varchar(1024) DEFAULT NULL,
  `patient_age` varchar(1024) DEFAULT NULL,
  `operation_name` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_studies`
--

INSERT INTO `tbl_studies` (`ID`, `patient_id`, `centre_id`, `study_uid`, `study_date`, `study_description`, `from_rdsr`, `patient_size`, `patient_weight`, `patient_age`, `operation_name`, `add_date`) VALUES
(1, 12345, 12345, '124568', '2018-09-08', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', '2017-03-21 17:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_study_attributes`
--

CREATE TABLE `tbl_study_attributes` (
  `ID` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `protocol_id` bigint(20) DEFAULT NULL,
  `exposures_count` varchar(1024) DEFAULT NULL,
  `total_dlp_mgycm` varchar(1024) DEFAULT NULL,
  `report_series_no` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_study_attributes`
--

INSERT INTO `tbl_study_attributes` (`ID`, `study_id`, `protocol_id`, `exposures_count`, `total_dlp_mgycm`, `report_series_no`, `add_date`) VALUES
(1, 123456, 123456, 'Test', 'Test', 'Test', '2017-03-21 17:07:16');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_study_exposures`
--

CREATE TABLE `tbl_study_exposures` (
  `ID` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `phantom_id` bigint(20) NOT NULL,
  `acq_type_id` bigint(20) NOT NULL,
  `dlp_mgycm` varchar(1024) DEFAULT NULL,
  `ctd_vol` varchar(1024) DEFAULT NULL,
  `kvp` varchar(1024) DEFAULT NULL,
  `tube_current` varchar(1024) DEFAULT NULL,
  `exposure_time` varchar(1024) DEFAULT NULL,
  `scan_length` varchar(1024) DEFAULT NULL,
  `single_collim_width` varchar(1024) DEFAULT NULL,
  `total_collim_width` varchar(1024) DEFAULT NULL,
  `spital_pitch` varchar(1024) DEFAULT NULL,
  `body_part` varchar(1024) DEFAULT NULL,
  `slice_thickness` varchar(1024) DEFAULT NULL,
  `acq_protocol` varchar(1024) DEFAULT NULL,
  `procedure_context` varchar(1024) DEFAULT NULL,
  `comments` varchar(1024) DEFAULT NULL,
  `topz_loc_scanning` varchar(1024) DEFAULT NULL,
  `bottomz_loc_scanning` varchar(1024) DEFAULT NULL,
  `topz_loc_vol` varchar(1024) DEFAULT NULL,
  `bottomz_loc_vol` varchar(1024) DEFAULT NULL,
  `exposurescol` varchar(1024) DEFAULT NULL,
  `xray_source_count` varchar(1024) DEFAULT NULL,
  `identifier` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_study_exposures`
--

INSERT INTO `tbl_study_exposures` (`ID`, `study_id`, `phantom_id`, `acq_type_id`, `dlp_mgycm`, `ctd_vol`, `kvp`, `tube_current`, `exposure_time`, `scan_length`, `single_collim_width`, `total_collim_width`, `spital_pitch`, `body_part`, `slice_thickness`, `acq_protocol`, `procedure_context`, `comments`, `topz_loc_scanning`, `bottomz_loc_scanning`, `topz_loc_vol`, `bottomz_loc_vol`, `exposurescol`, `xray_source_count`, `identifier`, `add_date`) VALUES
(1, 12345, 12345, 12345, 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', 'Test', '2017-03-21 17:07:43');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_usermeta`
--

CREATE TABLE `tbl_usermeta` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `meta_key` text NOT NULL,
  `meta_value` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `ID` bigint(20) NOT NULL,
  `user_email` varchar(512) NOT NULL,
  `user_pass` varchar(512) NOT NULL,
  `first_name` varchar(256) NOT NULL,
  `last_name` varchar(256) NOT NULL,
  `user_role` varchar(256) NOT NULL,
  `user_status` bigint(20) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `registered_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`ID`, `user_email`, `user_pass`, `first_name`, `last_name`, `user_role`, `user_status`, `created_by`, `registered_at`) VALUES
(1, 'admin@admin.com', '21232f297a57a5a743894a0e4a801fc3', 'Mazen', 'Sehgal', 'admin', 1, 0, '2017-03-05 17:43:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_xray_sources`
--

CREATE TABLE `tbl_xray_sources` (
  `ID` bigint(20) NOT NULL,
  `exposure_id` bigint(20) NOT NULL,
  `identifier` varchar(1024) DEFAULT NULL,
  `kvp` varchar(1024) DEFAULT NULL,
  `tube_current_ma` varchar(1024) DEFAULT NULL,
  `max_tube_current_ma` varchar(1024) DEFAULT NULL,
  `exposure_t_per_rotations` varchar(1024) DEFAULT NULL,
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_xray_sources`
--

INSERT INTO `tbl_xray_sources` (`ID`, `exposure_id`, `identifier`, `kvp`, `tube_current_ma`, `max_tube_current_ma`, `exposure_t_per_rotations`, `add_date`) VALUES
(1, 1234, 'Test', 'Test', 'Test', 'Test', 'Test', '2017-03-05 18:34:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_access_log`
--
ALTER TABLE `tbl_access_log`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_acquisition_types`
--
ALTER TABLE `tbl_acquisition_types`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_centres`
--
ALTER TABLE `tbl_centres`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_dose_alerts`
--
ALTER TABLE `tbl_dose_alerts`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_image_dose_attributes`
--
ALTER TABLE `tbl_image_dose_attributes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_image_dose_attribute_descriptions`
--
ALTER TABLE `tbl_image_dose_attribute_descriptions`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_modalities`
--
ALTER TABLE `tbl_modalities`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_options`
--
ALTER TABLE `tbl_options`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_patients`
--
ALTER TABLE `tbl_patients`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_phantoms`
--
ALTER TABLE `tbl_phantoms`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_protocol`
--
ALTER TABLE `tbl_protocol`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_protocol_specs`
--
ALTER TABLE `tbl_protocol_specs`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_series`
--
ALTER TABLE `tbl_series`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_studies`
--
ALTER TABLE `tbl_studies`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_study_attributes`
--
ALTER TABLE `tbl_study_attributes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_study_exposures`
--
ALTER TABLE `tbl_study_exposures`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_usermeta`
--
ALTER TABLE `tbl_usermeta`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbl_xray_sources`
--
ALTER TABLE `tbl_xray_sources`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_access_log`
--
ALTER TABLE `tbl_access_log`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_acquisition_types`
--
ALTER TABLE `tbl_acquisition_types`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_centres`
--
ALTER TABLE `tbl_centres`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_dose_alerts`
--
ALTER TABLE `tbl_dose_alerts`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_equipments`
--
ALTER TABLE `tbl_equipments`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_image_dose_attributes`
--
ALTER TABLE `tbl_image_dose_attributes`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_image_dose_attribute_descriptions`
--
ALTER TABLE `tbl_image_dose_attribute_descriptions`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_modalities`
--
ALTER TABLE `tbl_modalities`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_notifications`
--
ALTER TABLE `tbl_notifications`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;
--
-- AUTO_INCREMENT for table `tbl_options`
--
ALTER TABLE `tbl_options`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbl_patients`
--
ALTER TABLE `tbl_patients`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_phantoms`
--
ALTER TABLE `tbl_phantoms`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_protocol`
--
ALTER TABLE `tbl_protocol`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_protocol_specs`
--
ALTER TABLE `tbl_protocol_specs`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_series`
--
ALTER TABLE `tbl_series`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_studies`
--
ALTER TABLE `tbl_studies`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_study_attributes`
--
ALTER TABLE `tbl_study_attributes`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_study_exposures`
--
ALTER TABLE `tbl_study_exposures`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_usermeta`
--
ALTER TABLE `tbl_usermeta`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10000808152;
--
-- AUTO_INCREMENT for table `tbl_xray_sources`
--
ALTER TABLE `tbl_xray_sources`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
