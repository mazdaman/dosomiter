<?php
// define site details
global $db;

define( 'INC_URL' , site_url() . '/' . INC );
define( 'CONTENT_URL' , site_url() . '/' . CONTENT );
define( 'CSS_URL' , site_url() . '/' . CONTENT .'/assets/css/' );
define( 'JS_URL' , site_url() . '/' . CONTENT .'/assets/js/' );
define( 'IMAGE_URL' , site_url() . '/' . CONTENT .'/assets/img/' );
define( 'PROCESS_URL' , site_url() . '/' . INC .'/process.php' );


define( 'BLOG_NAME' ,'Equip Management' );
define( 'ADMIN_EMAIL' ,'info@prospectstrails.com' );
define( 'NO_REPLY' ,'no-reply@prospectstrails.com' );


define( 'TBL_USERS' , $db->prefix. 'users' );
define( 'TBL_USERMETA' , $db->prefix. 'usermeta' );
define( 'TBL_OPTION' , $db->prefix. 'options' );
define( 'TBL_ACCESS_LOG' , $db->prefix. 'access_log' );
define( 'TBL_NOTIFICATIONS' , $db->prefix. 'notifications' );
define( 'TBL_CENTRES' , $db->prefix. 'centres' );
define( 'TBL_ACQUISITION_TYPES' , $db->prefix. 'acquisition_types' );
define( 'TBL_STUDY_ATTRIBUTES' , $db->prefix. 'study_attributes' );
define( 'TBL_STUDY_EXPOSURES' , $db->prefix. 'study_exposures' );
define( 'TBL_EQUIPMENTS' , $db->prefix. 'equipments' );
define( 'TBL_IMAGES' , $db->prefix. 'images' );
define( 'TBL_IMAGE_DOSE_ATTRIBUTES' , $db->prefix. 'image_dose_attributes' );
define( 'TBL_IMAGE_DOSE_ATTRIBUTE_DESCRIPTIONS' , $db->prefix. 'image_dose_attribute_descriptions' );
define( 'TBL_MODALITIES' , $db->prefix. 'modalities' );
define( 'TBL_PATIENTS' , $db->prefix. 'patients' );
define( 'TBL_PHANTOMS' , $db->prefix. 'phantoms' );
define( 'TBL_SERIES' , $db->prefix. 'series' );
define( 'TBL_STUDIES' , $db->prefix. 'studies' );
define( 'TBL_XRAY_SOURCES' , $db->prefix. 'xray_sources' );
define( 'TBL_PROTOCOLS' , $db->prefix. 'protocol' );
define( 'TBL_PROTOCOL_SPECS' , $db->prefix. 'protocol_specs' );
define( 'TBL_DOSE_ALERTS' , $db->prefix. 'dose_alerts' );
?>