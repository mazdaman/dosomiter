<?php 
// if accessed directly than exit
if (!defined('ABSPATH')) exit;

if( !class_exists('Header') ):
	class Header {
		private $database;
		private $profile;
		function __construct() {
			global $db,$Profile;
			$this->database = $db;
			$this->profile = $Profile;
		}
		
		public function head(){
			ob_start();
			?>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
			<!-- Meta, title, CSS, favicons, etc. -->
			<meta charset="utf-8"/>
			<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
			<meta name="viewport" content="width=device-width, initial-scale=1"/>

			<!-- Bootstrap -->
			<link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700' rel='stylesheet' type='text/css' />
			<link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'/>
			<link href="<?php echo CSS_URL;?>bootstrap.min.css" rel="stylesheet"/>
			<!-- jQuery ui-->
			<link href="<?php echo CSS_URL;?>jquery-ui.css" rel="stylesheet"/>
			<!-- Font Awesome -->
			<link href="<?php echo CSS_URL;?>font-awesome.min.css" rel="stylesheet"/>
			<!-- iCheck -->
			<link href="<?php echo CSS_URL;?>green.css" rel="stylesheet"/>
			<!-- bootstrap-progressbar -->
			<link href="<?php echo CSS_URL;?>bootstrap-progressbar-3.3.4.min.css" rel="stylesheet"/>
			<!-- jVectorMap -->
			<link href="<?php echo CSS_URL;?>jquery-jvectormap-2.0.3.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>dataTables.bootstrap.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>buttons.bootstrap.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>fixedHeader.bootstrap.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>responsive.bootstrap.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>scroller.bootstrap.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>prettify.min.css" rel="stylesheet"/>
			<!-- Select2 -->
			<link href="<?php echo CSS_URL;?>select2.min.css" rel="stylesheet"/>
			<!-- Switchery -->
			<link href="<?php echo CSS_URL;?>switchery.min.css" rel="stylesheet"/>
			<!-- starrr -->
			<link href="<?php echo CSS_URL;?>starrr.css" rel="stylesheet"/>
			<!-- P Notify -->
			<link href="<?php echo CSS_URL;?>pnotify.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>pnotify.buttons.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>pnotify.nonblock.css" rel="stylesheet"/>
			<!--Full Calendar-->
			<link href="<?php echo CSS_URL;?>fullcalendar.min.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>fullcalendar.print.css" rel="stylesheet" media="print"/>
			<!-- Custom Theme Style -->
			<link href="<?php echo CSS_URL;?>custom.css" rel="stylesheet"/>
			<link href="<?php echo CSS_URL;?>styles.css" rel="stylesheet"/>

			<script>
				var site_url = '<?php echo site_url();?>';
				var ajax_url = '<?php echo PROCESS_URL;?>';
			</script>
			<?php echo $this->scripts(); ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}
		
		public function scripts(){
			ob_start();
			?>
			<!-- jQuery -->
			<script src="<?php echo JS_URL;?>jquery.min.js"></script>
			<!-- jQuery -->
			<script src="<?php echo JS_URL;?>jquery-ui.js"></script>
			<!-- canvas dot -->
			<script src="<?php echo JS_URL;?>canvasdots.js"></script>
			<!-- Bootstrap -->
			<script src="<?php echo JS_URL;?>bootstrap.min.js"></script>
			<!-- FastClick -->
			<script src="<?php echo JS_URL;?>fastclick.js"></script>
			<!-- NProgress -->
			<script src="<?php echo JS_URL;?>nprogress.js"></script>
			 <!-- morris.js -->
			<script src="<?php echo JS_URL;?>raphael.min.js"></script>
			<script src="<?php echo JS_URL;?>morris.min.js"></script>
			<!-- Chart.js -->
			<script src="<?php echo JS_URL;?>Chart.min.js"></script>
			<!-- gauge.js -->
			<script src="<?php echo JS_URL;?>gauge.min.js"></script>
			<!-- bootstrap-progressbar -->
			<script src="<?php echo JS_URL;?>bootstrap-progressbar.min.js"></script>
			<!-- iCheck -->
			<script src="<?php echo JS_URL;?>icheck.min.js"></script>
			<!-- Skycons -->
			<script src="<?php echo JS_URL;?>skycons.js"></script>
			<!-- Flot -->
			<script src="<?php echo JS_URL;?>jquery.flot.js"></script>
			<script src="<?php echo JS_URL;?>jquery.flot.pie.js"></script>
			<script src="<?php echo JS_URL;?>jquery.flot.time.js"></script>
			<script src="<?php echo JS_URL;?>jquery.flot.stack.js"></script>
			<script src="<?php echo JS_URL;?>jquery.flot.resize.js"></script>
			<!-- Flot plugins -->
			<script src="<?php echo JS_URL;?>jquery.flot.orderBars.js"></script>
			<script src="<?php echo JS_URL;?>date.js"></script>
			<script src="<?php echo JS_URL;?>jquery.flot.spline.js"></script>
			<script src="<?php echo JS_URL;?>curvedLines.js"></script>
			<!-- jVectorMap -->
			<script src="<?php echo JS_URL;?>jquery-jvectormap-2.0.3.min.js"></script>
			<!-- bootstrap-daterangepicker -->
			<script src="<?php echo JS_URL;?>moment.min.js"></script>
			<script src="<?php echo JS_URL;?>fullcalendar.min.js"></script>
			<script src="<?php echo JS_URL;?>daterangepicker.js"></script>
			<!-- datatables -->
			 <script src="<?php echo JS_URL;?>jquery.dataTables.min.js"></script>
			<script src="<?php echo JS_URL;?>dataTables.bootstrap.min.js"></script>
			<script src="<?php echo JS_URL;?>dataTables.buttons.min.js"></script>
			<script src="<?php echo JS_URL;?>buttons.bootstrap.min.js"></script>
			<script src="<?php echo JS_URL;?>buttons.flash.min.js"></script>
			<script src="<?php echo JS_URL;?>buttons.html5.min.js"></script>
			<script src="<?php echo JS_URL;?>buttons.print.min.js"></script>
			<script src="<?php echo JS_URL;?>dataTables.fixedHeader.min.js"></script>
			<script src="<?php echo JS_URL;?>dataTables.keyTable.min.js"></script>
			<script src="<?php echo JS_URL;?>dataTables.responsive.min.js"></script>
			<script src="<?php echo JS_URL;?>responsive.bootstrap.js"></script>
			<script src="<?php echo JS_URL;?>datatables.scroller.min.js"></script>
			<script src="<?php echo JS_URL;?>jszip.min.js"></script>
			<script src="<?php echo JS_URL;?>pdfmake.min.js"></script>
			<script src="<?php echo JS_URL;?>vfs_fonts.js"></script>
			 <script src="<?php echo JS_URL;?>bootstrap-wysiwyg.min.js"></script>
			<script src="<?php echo JS_URL;?>jquery.hotkeys.js"></script>
			<script src="<?php echo JS_URL;?>prettify.js"></script>
			<!-- jQuery Tags Input -->
			<script src="<?php echo JS_URL;?>jquery.tagsinput.js"></script>
			<!-- Switchery -->
			<script src="<?php echo JS_URL;?>switchery.min.js"></script>
			<!-- Select2 -->
			<script src="<?php echo JS_URL;?>select2.full.min.js"></script>
			<!-- Parsley -->
			<script src="<?php echo JS_URL;?>parsley.min.js"></script>
			<!-- Autosize -->
			<script src="<?php echo JS_URL;?>autosize.min.js"></script>
			<!-- jQuery autocomplete -->
			<script src="<?php echo JS_URL;?>jquery.autocomplete.min.js"></script>
			<!-- starrr -->
			<script src="<?php echo JS_URL;?>starrr.js"></script>
			<!-- p notify -->
			<script src="<?php echo JS_URL;?>pnotify.js"></script>
			<script src="<?php echo JS_URL;?>pnotify.buttons.js"></script>
			<script src="<?php echo JS_URL;?>pnotify.nonblock.js"></script>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function header(){
			ob_start();
			echo $this->sidebar();
			echo $this->top_bar();
			$content = ob_get_clean();
			return $content;
		}
		
		public function sidebar(){
			ob_start();
			?>
			<!--Sidebar start-->
			<div class="col-md-3 left_col">
				<div class="left_col scroll-view">
					<div class="navbar nav_title" style="border: 0;">
						<a href="<?php echo site_url().'/dashboard/';?>" class="site_title">
							<i class="fa fa-ravelry"></i> <span><?php echo get_site_name();?></span>
						</a>
					</div>

					<div class="clearfix"></div>

					<!-- menu profile quick info -->
					<div class="profile">
						<div class="profile_pic">
							<img src="<?php echo get_current_user_profile_image();?>" alt="..." class="img-circle profile_img">
						</div>
						<div class="profile_info">
							<span>Welcome,</span>
							<h2><?php echo get_current_user_name();?></h2>
						</div>
					</div>
					<!-- /menu profile quick info -->
					
					<div class="clearfix"></div>
					<!-- sidebar menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<!--<h3>General</h3>-->
							<ul class="nav side-menu">
								<li>
									<a><i class="fa fa-home"></i> Home <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo site_url().'/dashboard/';?>">Dashboard</a></li>
										<li><a href="<?php echo site_url();?>/notifications/">Notification 
										<?php $notifications_count = get_unread_notification_count(); if($notifications_count > 0): ?>
										<span class="label label-success">New</span>
										<?php endif; ?>
										</a></li>
									</ul>
								</li>
								
								<li>
									<a><i class="fa fa-trello"></i> My Profile <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo site_url();?>/edit-profile/">Edit Profile</a></li>
										<li><a href="<?php echo site_url();?>/change-password/">Change Password</a></li>
										<li><a href="<?php echo site_url().'/access-log/';?>">Access Log</a></li>
									</ul>
								</li>
								
								<?php if( user_can('view_user') || user_can('edit_user') || user_can('add_user')): ?>
								<li>
									<a><i class="fa fa-user"></i>Users <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<?php if( user_can('view_user') ): ?>
										<li><a href="<?php echo site_url().'/users/';?>">All Users</a></li>
										<?php endif;?>
										<?php if( user_can('edit_user') || user_can('add_user')): ?>
										<li><a href="<?php echo site_url();?>/add-new-user/">Add New User</a></li>
										<li class="hidden-xs hidden-lg"><a href="<?php echo site_url();?>/edit-user/"></a></li>
										<?php endif;?>
										<?php if( user_can('view_user') ): ?>
										<li class="hidden-xs hidden-lg"><a href="<?php echo site_url();?>/view-user/"></a></li>
										<?php endif;?>
									</ul>
								</li>
								<?php endif; ?>
								
								<li>
									<a><i class="fa fa-cubes"></i> Management<span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<?php 
										$menus = $this->menus();
											if(!empty($menus)):
												foreach($menus as $key => $menu):
													$menu = (object)$menu;
													if(user_can('view_'.$key)):
														echo '<li><a href="'.site_url().'/'.$menu->slug.'/">'.$menu->label.'</a></li>';
													endif;
												endforeach;
											endif;
										?>
									</ul>
								</li>
								
								<?php if(is_admin()): ?>
								<li>
									<a><i class="fa fa-cog"></i> Setting <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo site_url();?>/general-setting/">General</a></li>
										<li><a href="<?php echo site_url();?>/manage-roles/">Manage Roles</a></li>
									</ul>
								</li>
								<?php endif; ?>
							</ul>
						</div>
					</div>
					<!-- /sidebar menu -->
				</div>
			</div><!--Sidebar end-->
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function top_bar(){
			ob_start();
			?>
			<div class="top_nav">
				<div class="nav_menu">
					<nav class="" role="navigation">
						<div class="nav toggle">
							<a id="menu_toggle"><i class="fa fa-bars"></i></a>
						</div>

						<ul class="nav navbar-nav navbar-right">
							<li class="">
							 	<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<img src="<?php echo get_current_user_profile_image();?>" alt=""><?php echo get_current_user_name();?>
									<span class=" fa fa-angle-down"></span>
								</a>
								 <ul class="dropdown-menu dropdown-usermenu pull-right">
									<li><a href="<?php echo site_url();?>/profile/">  Profile</a></li>
									<li><a href="<?php echo site_url();?>/change-password/">  Change Password</a></li>
									<li><a href="javascript:;">Help</a></li>
									<li><a href="<?php echo site_url();?>/logout/" class="link-logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
								</ul>
							</li>

							<?php echo $this->profile->notifications__top__bar(); ?>
						</ul>
					</nav>
				</div>
			  </div>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function page__header($title,$status = true){
			ob_start();
			?>
			<div class="page-title">
				<div class="title_left">
					<h3><?php _e($title);?></h3>
				</div>
				
				<?php if($status === true): ?>
				<div class="title_right">
					<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for...">
							<span class="input-group-btn">
								<button class="btn btn-default" type="button">Go!</button>
							</span>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<div class="clearfix"></div>
			<?php
			$content = ob_get_clean();
			return $content;
		}
		
		public function home__page__header(){
			ob_start();
			?>
			<nav>
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h1><?php echo get_site_name();?></h1>
							<p><?php echo get_site_description();?></p>	
						</div>
					</div>
				</div>
			</nav>
			<?php
			$content = ob_get_clean();
			return $content;
		}
		
		private function menus(){
			$menus = array(
				'acquisition_types' => array(
					'slug' => __('acquisition-types'),
					'label' => __('Acquisition Types')
				),
				'centres' => array(
					'slug' => __('centres'),
					'label' => __('Centres')
				),
				'equipments' => array(
					'slug' => __('equipments'),
					'label' => __('Equipments')
				),
				'images' => array(
					'slug' => __('images'),
					'label' => __('Images')
				),
				'images' => array(
					'slug' => __('images'),
					'label' => __('Images')
				),
				'image_dose_attributes' => array(
					'slug' => __('image-dose-attributes'),
					'label' => __('Image Dose Attributes')
				),
				'image_dose_attribute_descriptions' => array(
					'slug' => __('image-dose-attribute-descriptions'),
					'label' => __('Image Dose Attribute Descriptions')
				),
				'modalities' => array(
					'slug' => __('modalities'),
					'label' => __('Modalities')
				),
				'patients' => array(
					'slug' => __('patients'),
					'label' => __('Patients')
				),
				'phantoms' => array(
					'slug' => __('phantoms'),
					'label' => __('Phantoms')
				),
				'series' => array(
					'slug' => __('series'),
					'label' => __('Series')
				),
				'studies' => array(
					'slug' => __('studies'),
					'label' => __('Studies')
				),
				'study_attributes' => array(
					'slug' => __('study-attributes'),
					'label' => __('Study attributes')
				),
				'study_exposures' => array(
					'slug' => __('study-exposures'),
					'label' => __('Study Exposures')
				),
				'xray_sources' => array(
					'slug' => __('xray-sources'),
					'label' => __('Xray Sources')
				),
				'protocols' => array(
					'slug' => __('protocols'),
					'label' => __('Protocols')
				),
				'protocol_specs' => array(
					'slug' => __('protocol-specs'),
					'label' => __('Protocol Specs')
				),
				'dose_alerts' => array(
					'slug' => __('dose-alerts'),
					'label' => __('Dose Alerts')
				),
			);
			
			return $menus;
		}
	}
endif;
?>