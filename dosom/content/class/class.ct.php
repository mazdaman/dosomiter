<?php 
// if accessed directly than exit
if (!defined('ABSPATH')) exit;

if( !class_exists('CT') ):
	class CT{
		private $database;
		private $current__user__id;
		private $current__user;
		function __construct() {
			global $db;
			$this->database = $db;
			$this->current__user__id = get_current_user_id();
			$this->current__user = get_userdata($this->current__user__id);
		}
		
		public function all__acquisition__types__page(){
			ob_start();
			$results = get_tabledata(TBL_ACQUISITION_TYPES,false);

			if( !user_can('view_acquisition_types') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no acquisition types record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__phantoms__page(){
			ob_start();
			$results = get_tabledata(TBL_PHANTOMS,false);

			if( !user_can('view_phantoms') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no phantoms record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Size');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php _e($row->size);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__centres__page(){
			ob_start();
			$results = get_tabledata(TBL_CENTRES,false);

			if( !user_can('view_centres') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no centres record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__patients__page(){
			ob_start();
			$results = get_tabledata(TBL_PATIENTS,false);

			if( !user_can('view_patients') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no patients record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Hospital Patient ID');?></th>
							<th><?php _e('Gender');?></th>
							<th><?php _e('Year of Birth');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->hospital_id);?></td>
							<td><?php _e($row->gender);?></td>
							<td><?php _e($row->yob);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__modalities__page(){
			ob_start();
			$results = get_tabledata(TBL_MODALITIES,false);

			if( !user_can('view_modalities') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no modalities record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__images__page(){
			ob_start();
			$results = get_tabledata(TBL_IMAGES,false);

			if( !user_can('view_images') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no images record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Series ID');?></th>
							<th><?php _e('UID');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->series_id);?></td>
							<td><?php _e($row->image_uid);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__image__dose__attributes__page(){
			ob_start();
			$results = get_tabledata(TBL_IMAGE_DOSE_ATTRIBUTES,false);

			if( !user_can('view_image_dose_attributes') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no image dose attributes record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Image ID');?></th>
							<th><?php _e('Att Type');?></th>
							<th><?php _e('Value');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->image_id);?></td>
							<td><?php _e($row->att_type);?></td>
							<td><?php _e($row->value);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}
	
		public function all__image__dose__attribute__descriptions__page(){
			ob_start();
			$results = get_tabledata(TBL_IMAGE_DOSE_ATTRIBUTE_DESCRIPTIONS,false);

			if( !user_can('view_image_dose_attribute_descriptions') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no image dose attributes descriptions record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Description');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->description);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__xray__sources__page(){
			ob_start();
			$results = get_tabledata(TBL_XRAY_SOURCES,false);

			if( !user_can('view_xray_sources') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no xray sources record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Exposure ID');?></th>
							<th><?php _e('Identifier');?></th>
							<th><?php _e('KVP');?></th>
							<th><?php _e('Tube Current MA');?></th>
							<th><?php _e('Max Tube Current MA');?></th>
							<th><?php _e('Exposure T Per Rotations');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->exposure_id);?></td>
							<td><?php _e($row->identifier);?></td>
							<td><?php _e($row->kvp);?></td>
							<td><?php _e($row->tube_current_ma);?></td>
							<td><?php _e($row->max_tube_current_ma);?></td>
							<td><?php _e($row->exposure_t_per_rotations);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__series__page(){
			ob_start();
			$results = get_tabledata(TBL_SERIES,false);

			if( !user_can('view_series') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no series record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Study ID');?></th>
							<th><?php _e('Equipment ID');?></th>
							<th><?php _e('Series UID');?></th>
							<th><?php _e('Date');?></th>
							<th><?php _e('Description');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->study_id);?></td>
							<td><?php _e($row->equipment_id);?></td>
							<td><?php _e($row->series_uid);?></td>
							<td><?php echo date('M d,Y',strtotime($row->series_date));?></td>
							<td><?php _e($row->description);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__equipments__page(){
			ob_start();
			$results = get_tabledata(TBL_EQUIPMENTS,false);

			if( !user_can('view_equipments') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no equipments record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Modality');?></th>
							<th><?php _e('Manufacturer');?></th>
							<th><?php _e('Model');?></th>
							<th><?php _e('Serial NO');?></th>
							<th><?php _e('Software Version');?></th>
							<th><?php _e('Station Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->modality_id);?></td>
							<td><?php _e($row->manufacturer);?></td>
							<td><?php _e($row->model);?></td>
							<td><?php _e($row->serial_no);?></td>
							<td><?php _e($row->software_version);?></td>
							<td><?php _e($row->station_name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__studies__page(){
			ob_start();
			$results = get_tabledata(TBL_STUDIES,false);

			if( !user_can('view_studies') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no studies record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Patient ID');?></th>
							<th><?php _e('Centre ID');?></th>
							<th><?php _e('Study UID');?></th>
							<th><?php _e('Study Date');?></th>
							<th><?php _e('Description');?></th>
							<th><?php _e('From RDSR');?></th>		
							<th><?php _e('Patient Size');?></th>
							<th><?php _e('Patient Weight');?></th>
							<th><?php _e('Patient Age');?></th>
							<th><?php _e('Operation Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->patient_id);?></td>
							<td><?php _e($row->centre_id);?></td>
							<td><?php _e($row->study_uid);?></td>
							<td><?php echo date('M d,Y',strtotime($row->study_date));?></td>
							<td><?php _e($row->study_description);?></td>
							<td><?php _e($row->from_rdsr);?></td>
							<td><?php _e($row->patient_size);?></td>
							<td><?php _e($row->patient_weight);?></td>
							<td><?php _e($row->patient_age);?></td>
							<td><?php _e($row->operation_name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__study__attributes__page(){
			ob_start();
			$results = get_tabledata(TBL_STUDY_ATTRIBUTES,false);

			if( !user_can('view_study_attributes') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no study attributes record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Study ID');?></th>
							<th><?php _e('Protocol');?></th>
							<th><?php _e('Exposures Count');?></th>
							<th><?php _e('Total DLP Mgycm');?></th>
							<th><?php _e('Report Series NO');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->study_id);?></td>
							<td><?php _e($row->protocol_id);?></td>
							<td><?php _e($row->exposures_count);?></td>
							<td><?php _e($row->total_dlp_mgycm);?></td>
							<td><?php _e($row->report_series_no);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}
		
		public function all__study__exposures__page(){
			ob_start();
			$results = get_tabledata(TBL_STUDY_EXPOSURES,false);

			if( !user_can('view_study_exposures') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no study exposures record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Study ID');?></th>
							<th><?php _e('Phantom ID');?></th>
							<th><?php _e('Acq Type ID');?></th>
							<th><?php _e('DLP MGYCM');?></th>
							<th><?php _e('CTD vol');?></th>
							<th><?php _e('KVP');?></th>
							<th><?php _e('Tube Current');?></th>
							<th><?php _e('Exposure Time');?></th>
							<th><?php _e('Scan Length');?></th>
							<th><?php _e('Single Collim Width');?></th>
							<th><?php _e('Total Collim Width');?></th>
							<th><?php _e('Spital Pitch');?></th>
							<th><?php _e('Body Part');?></th>
							<th><?php _e('Slice Thickness');?></th>
							<th><?php _e('Acq Protocol');?></th>
							<th><?php _e('Procedure Context');?></th>
							<th><?php _e('Comments');?></th>
							<th><?php _e('TopZ loc Scanning');?></th>
							<th><?php _e('BottomZ loc Scanning');?></th>
							<th><?php _e('TopZ loc Vol');?></th>
							<th><?php _e('BottomZ loc Vol');?></th>
							<th><?php _e('Exposures Col');?></th>
							<th><?php _e('Xray Source Count');?></th>
							<th><?php _e('Identifier');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->study_id);?></td>
							<td><?php _e($row->phantom_id);?></td>
							<td><?php _e($row->acq_type_id);?></td>
							<td><?php _e($row->dlp_mgycm);?></td>
							<td><?php _e($row->ctd_vol);?></td>
							<td><?php _e($row->kvp);?></td>
							<td><?php _e($row->tube_current);?></td>
							<td><?php _e($row->exposure_time);?></td>
							<td><?php _e($row->scan_length);?></td>
							<td><?php _e($row->single_collim_width);?></td>
							<td><?php _e($row->total_collim_width);?></td>
							<td><?php _e($row->spital_pitch);?></td>
							<td><?php _e($row->body_part);?></td>
							<td><?php _e($row->slice_thickness);?></td>
							<td><?php _e($row->acq_protocol);?></td>
							<td><?php _e($row->procedure_context);?></td>
							<td><?php _e($row->comments);?></td>
							<td><?php _e($row->topz_loc_scanning);?></td>
							<td><?php _e($row->bottomz_loc_scanning);?></td>
							<td><?php _e($row->topz_loc_vol);?></td>
							<td><?php _e($row->bottomz_loc_vol);?></td>
							<td><?php _e($row->exposurescol);?></td>
							<td><?php _e($row->xray_source_count);?></td>
							<td><?php _e($row->identifier);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__protocols__page(){
			ob_start();
			$results = get_tabledata(TBL_PROTOCOLS,false);

			if( !user_can('view_protocols') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no protocol record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Protocol  Spec');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->protocol_spec_id);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}
		
		public function all__protocol__specs__page(){
			ob_start();
			$results = get_tabledata(TBL_PROTOCOL_SPECS,false);

			if( !user_can('view_protocol_specs') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no protocol spec record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}

		public function all__dose__alerts__page(){
			ob_start();
			$results = get_tabledata(TBL_DOSE_ALERTS,false);

			if( !user_can('view_dose_alerts') ):
				echo page_not_found('Oops ! You are not allowed to view this page.','Please check other pages !');
			elseif(!$results):
				echo page_not_found("Oops! There is no dose alerts record found",'  ',false);
			else:
			?>
				<table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap datatable-buttons" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th><?php _e('ID');?></th>
							<th><?php _e('Centre');?></th>
							<th><?php _e('Protocol  Spec');?></th>
							<th><?php _e('Name');?></th>
							<th><?php _e('Description');?></th>
							<th><?php _e('Value');?></th>
							<th><?php _e('Created On');?></th>
						</tr>
					</thead>
					<tbody>
						<?php if($results): foreach($results as $row): ?>
						<tr>
							<td><?php _e($row->ID);?></td>
							<td><?php _e($row->centre_id);?></td>
							<td><?php _e($row->protocol_spec_id);?></td>
							<td><?php _e($row->name);?></td>
							<td><?php _e($row->description);?></td>
							<td><?php _e($row->value);?></td>
							<td><?php echo date('M d,Y',strtotime($row->add_date));?></td>
						</tr>
						<?php endforeach; endif; ?>
					</tbody>
				</table>
			<?php endif; ?>
			<?php
			$content = ob_get_clean();
			return $content;
		}
	}
endif;
?>